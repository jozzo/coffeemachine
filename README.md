**Coffee Machine - Central Digitális Média**

      - Amit érdemesnek találsz, azt írd ki a gép console-jára [printConsoleLog()]
      - A készülék inicializálásakor írja ki a minimum bedobandó összeget
            (Espresso ára extrák nélkül)
      - A kávégép akár inicializáláskor rögtön fogadhasson "kezdőösszeget".
            Ez esetben csak akkor jelenjen meg a Minimálisan bedobandó szöveg,
            ha az összeg nem éri el azt
      - Csak addig lehessen kávét kérni, amig van rá keret
      - Egyszerre ne lehessen több kávét "legyártani"
      - Egy kávé elkészítésének ideje hozzávalónként 1 sec legyen
      - Valósítsd meg, hogy az Espresso fogadhasson extrákat (AcceptsExtras interface)
      - Extra hozzáadása kávéhoz a CoffeeMachine::addExtra() metódusával valósuljon meg
      - Lehessen tejszínt (Whip) is, mint extrát kérni a kávéba, mert jelenleg ilyen még nem létezik
      - Készíts egy raktárt, amiben az extra hozzávalók vannak. (Sugar, Milk, Whip).
            Egyszerű osztály elegendő, nem kell perzisztens tárolás.
            Csak egy raktár létezhet egy időben, az alkalmazás működése alatt nem töltődhet újra a készlet.
            Kezdő készlet minden hozzávalóból 3 darab.
            A GroundCoffee és a Water nem extra, azokra tekints úgy, mint kifogyhatatlan hozzávalók.
      - Az egyes hozzávalókat a raktárból vegyük ki, ha elfogy, akkor a Store dobjon egy
            Exception-t a megfelelő üzenettel. Ezt az Kávéautomata kezelje le.
            Minden Store készletváltozást logolj ki a console-ra
      - Kávé készítése közben írja ki a gép, hogy milyen hozzávalók kerülnek bele a rendelt kávéba.
      - Hozz létre egy új kávétípust (Cappuccino). Hozzávalói: Water, GroundCoffee, Milk, Whip
            Kösd be a kezelőfelületre, hogy rendelhető legyen.
            Ez a kávétípus ne fogadhasson extrákat, de ügyelj rá, hogy a Milk és a Whip a raktárból kerül ki.
            Az ára fixen 200 HUF legyen, a Milk és a Whip ára ne befolyásolja azt.
      - Módosítsd a "+ EXTRA" gombot, hogy csak akkor legyen kattintható, ha extra
            fogadására alkalmas kávé van kiválasztva.
            Ehhez használd az EventBus library-t, hasonlóan, mint a print() metódusoknál.
            http://greenrobot.org/eventbus/documentation/
      - (Bónusz) van egy Bug az Espresso és az extrák hozzáadása környékén. Ezt kéne javítani.