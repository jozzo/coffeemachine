package hu.centralmediacsoport.coffeemachine.exceptions;

import hu.centralmediacsoport.coffeemachine.Application;
import hu.centralmediacsoport.coffeemachine.R;

/**
 * Created by jozsa on 2017.01.01..
 */

public class StoreProductException extends Exception {
    @Override
    public String getMessage() {
        return Application.getContext().getString(R.string.extra_exception_sold_out);
    }


}
