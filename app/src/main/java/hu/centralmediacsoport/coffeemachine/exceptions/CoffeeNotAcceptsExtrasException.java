package hu.centralmediacsoport.coffeemachine.exceptions;

import hu.centralmediacsoport.coffeemachine.Application;
import hu.centralmediacsoport.coffeemachine.R;

/**
 * Created by david.eperjes on 08/11/16.
 */

public class CoffeeNotAcceptsExtrasException extends Exception {

    @Override
    public String getMessage() {
        return Application.getContext().getString(R.string.extra_exception_message);
    }
}
