package hu.centralmediacsoport.coffeemachine.messages;

/**
 * Created by david.eperjes on 11/11/16.
 */

public class CoffeeMachineAcceptsExtrasForCurrentCoffeeMessage {

    private boolean canAcceptExtras = false;

    public CoffeeMachineAcceptsExtrasForCurrentCoffeeMessage(boolean canAcceptExtras) {
        this.canAcceptExtras = canAcceptExtras;
    }

    public boolean isCanAcceptExtras() {
        return canAcceptExtras;
    }
}
