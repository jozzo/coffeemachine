package hu.centralmediacsoport.coffeemachine.machine.ingredients.extras;

/**
 * Created by jozsa on 2017.01.01..
 */

public class Whip extends ExtraIngredient {

    public static final int EXTRA_COST = 40;

    @Override
    public int getCost() {
        return EXTRA_COST;
    }

    @Override
    public String getName() {
        return "Whip";
    }
}

