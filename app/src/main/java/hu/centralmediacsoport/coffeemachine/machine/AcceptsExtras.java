package hu.centralmediacsoport.coffeemachine.machine;

import java.util.List;

import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.ExtraIngredient;

/**
 * Created by david.eperjes on 08/11/16.
 */

public interface AcceptsExtras {

    List<ExtraIngredient> getExtras();
    void addExtra(ExtraIngredient extraIngredient);
    void removeExtra(ExtraIngredient extraIngredient);
    void removeExtras();

}
