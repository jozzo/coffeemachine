package hu.centralmediacsoport.coffeemachine.machine.ingredients.extras;

/**
 * Created by david.eperjes on 08/11/16.
 */

public class Sugar extends ExtraIngredient {

    public static final int EXTRA_COST = 40;

    @Override
    public int getCost() {
        return EXTRA_COST;
    }

    @Override
    public String getName() {
        return "Sugar";
    }
}
