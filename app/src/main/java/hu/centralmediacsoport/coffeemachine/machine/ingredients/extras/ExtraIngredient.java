package hu.centralmediacsoport.coffeemachine.machine.ingredients.extras;

import hu.centralmediacsoport.coffeemachine.machine.ingredients.Ingredient;

/**
 * Created by david.eperjes on 08/11/16.
 */

public abstract class ExtraIngredient extends Ingredient {

    public abstract int getCost();

}
