package hu.centralmediacsoport.coffeemachine.machine.coffees;

import java.util.ArrayList;
import java.util.List;

import hu.centralmediacsoport.coffeemachine.machine.ingredients.Ingredient;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.ExtraIngredient;

/**
 * Created by david.eperjes on 08/11/16.
 */

public abstract class Coffee {

    protected List<Ingredient> ingredients = new ArrayList<>();

    public abstract String getName();

    public abstract int getCost();

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public final String getIngredientsString() {
        String strIngredients = "";
        for (Ingredient ingredient : ingredients) {
            strIngredients += " + " + ingredient.getName();
        }
        return strIngredients;
    }

    public int getPreparingTime() {
        int ingredientsSize = ingredients.size();
        return ingredientsSize * 1000;
    }

    public int getTotalCost() {
        int totalCost = getCost();
        for (Ingredient ingredient : ingredients) {
            if (ingredient instanceof ExtraIngredient) {
                totalCost += ((ExtraIngredient) ingredient).getCost();
            }
        }
        return totalCost;
    }
}
