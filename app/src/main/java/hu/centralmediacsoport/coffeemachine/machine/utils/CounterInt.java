package hu.centralmediacsoport.coffeemachine.machine.utils;

/**
 * Created by jozsa on 2017.01.01..
 */

public class CounterInt {
    int value = 0;

    public CounterInt(int value) {
        this.value = value;
    }

    public void increment() {
        ++value;
    }

    public void decrease() {
        --value;
    }

    public int get() {
        return value;
    }
}
