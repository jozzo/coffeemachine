package hu.centralmediacsoport.coffeemachine.machine.coffees;

import hu.centralmediacsoport.coffeemachine.exceptions.StoreProductException;
import hu.centralmediacsoport.coffeemachine.machine.Store;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.GroundCoffee;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.Water;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Milk;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Whip;

/**
 * Created by jozsa on 2017.01.01..
 */

public class Cappuccino extends Coffee {

    public static final int COST = 200;

    public Cappuccino() {
        ingredients.add(new Water());
        ingredients.add(new GroundCoffee());
        //Milk, Whip Extra
        ingredients.add(new Whip());
        ingredients.add(new Milk());

    }

    @Override
    public String getName() {
        return "Cappuccino";
    }

    @Override
    public int getCost() {
        return COST;
    }

    @Override
    public String toString() {
        return getName() + getIngredientsString() + " " + getCost() + " Ft";
    }

}
