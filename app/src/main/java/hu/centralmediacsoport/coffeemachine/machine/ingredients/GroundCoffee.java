package hu.centralmediacsoport.coffeemachine.machine.ingredients;

/**
 * Created by david.eperjes on 11/11/16.
 */

public class GroundCoffee extends Ingredient {
    @Override
    public String getName() {
        return "GroundCoffee";
    }
}
