package hu.centralmediacsoport.coffeemachine.machine.ingredients;

import java.util.List;

/**
 * Created by david.eperjes on 08/11/16.
 */

public abstract class Ingredient {

    public abstract String getName();

}
