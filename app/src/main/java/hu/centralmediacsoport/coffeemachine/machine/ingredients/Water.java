package hu.centralmediacsoport.coffeemachine.machine.ingredients;

/**
 * Created by david.eperjes on 11/11/16.
 */

public class Water extends Ingredient {
    @Override
    public String getName() {
        return "Water";
    }
}
