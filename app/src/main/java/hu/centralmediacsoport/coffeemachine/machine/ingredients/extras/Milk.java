package hu.centralmediacsoport.coffeemachine.machine.ingredients.extras;

/**
 * Created by david.eperjes on 08/11/16.
 */

public class Milk extends ExtraIngredient {

    public static final int EXTRA_COST = 50;

    @Override
    public int getCost() {
        return EXTRA_COST;
    }

    @Override
    public String getName() {
        return "Milk";
    }


}
