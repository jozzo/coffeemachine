package hu.centralmediacsoport.coffeemachine.machine;

import android.os.Handler;
import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;

import hu.centralmediacsoport.coffeemachine.exceptions.CoffeeNotAcceptsExtrasException;
import hu.centralmediacsoport.coffeemachine.exceptions.StoreProductException;
import hu.centralmediacsoport.coffeemachine.machine.coffees.Coffee;
import hu.centralmediacsoport.coffeemachine.machine.coffees.Espresso;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.Ingredient;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.ExtraIngredient;
import hu.centralmediacsoport.coffeemachine.machine.utils.SoundManager;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineBalanceMessage;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineConsoleLogMessage;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineStatusMessage;

/**
 * Created by david.eperjes on 08/11/16.
 */

public class CoffeeMachine {

    /**
     * TODO: Amit érdemesnek találsz, azt írd ki a gép console-jára [printConsoleLog()]
     * TODO: A készülék inicializálásakor írja ki a minimum bedobandó összeget
     * (Espresso ára extrák nélkül)
     * TODO: A kávégép akár inicializáláskor rögtön fogadhasson "kezdőösszeget".
     * Ez esetben csak akkor jelenjen meg a Minimálisan bedobandó szöveg,
     * ha az összeg nem éri el azt
     * TODO: Csak addig lehessen kávét kérni, amig van rá keret
     * TODO: Egyszerre ne lehessen több kávét "legyártani"
     * TODO: Egy kávé elkészítésének ideje hozzávalónként 1 sec legyen
     * TODO: Valósítsd meg, hogy az Espresso fogadhasson extrákat (AcceptsExtras interface)
     * TODO: Lehessen tejszínt (Whip) is, mint extrát kérni a kávéba, mert jelenleg ilyen még nem létezik
     * TODO: Készíts egy raktárt, amiben az extra hozzávalók vannak. (Sugar, Milk, Whip).
     * Egyszerű osztály elegendő, nem kell perzisztens tárolás.
     * Csak egy raktár létezhet egy időben, az alkalmazás működése alatt nem töltődhet újra a készlet.
     * Kezdő készlet minden hozzávalóból 3 darab.
     * A GroundCoffee és a Water nem extra, azokra tekints úgy, mint kifogyhatatlan hozzávalók.
     * TODO: Az egyes hozzávalókat a raktárból vegyük ki, ha elfogy, akkor a Store dobjon egy
     * Exception-t a megfelelő üzenettel. Ezt az Kávéautomata kezelje le.
     * Minden Store készletváltozást logolj ki a console-ra
     * TODO: Kávé készítése közben írja ki a gép, hogy milyen hozzávalók kerülnek bele a rendelt kávéba.
     * TODO: Hozz létre egy új kávétípust (Cappuccino). Hozzávalói: Water, GroundCoffee, Milk, Whip
     * Kösd be a kezelőfelületre, hogy rendelhető legyen.
     * Ez a kávétípus ne fogadhasson extrákat, de ügyelj rá, hogy a Milk és a Whip a raktárból kerül ki.
     * Az ára fixen 200 HUF legyen, a Milk és a Whip ára ne befolyásolja azt.
     * TODO: Módosítsd a "+ EXTRA" gombot, hogy csak akkor legyen kattintható, ha extra
     * fogadására alkalmas kávé van kiválasztva.
     * Ehhez használd az EventBus library-t, hasonlóan, mint a print*() metódusoknál.
     * http://greenrobot.org/eventbus/documentation/
     * TODO: (Bónusz) van egy Bug az Espresso és az extrák hozzáadása környékén. Ezt kéne javítani.
     */

    private int mMoney = 0;
    private Coffee mCurrentCoffee;
    private boolean isCoffeeInProgress = false;

    public CoffeeMachine() {
        printConsoleLog("Initializing CoffeeMachine.");
        print("Welcome to CentralPresso!");
        print("Please give me at least " + Espresso.COST + " HUF!");
        printBalance();
    }

    public void pay(int money) {
        printConsoleLog("Adding " + money + " coins");
        mMoney += money;
        printBalance();
        SoundManager.playCoinSound();
    }

    public void payBack() {

        mCurrentCoffee = null;

        printConsoleLog("User wants his money back.");

        if (mMoney == 0) {
            printConsoleLog("User has no money in the machine.");
            print("No money left in the machine.");
            return;
        }

        int moneyBack = mMoney;
        mMoney = 0;
        printConsoleLog("Money back: " + moneyBack + " HUF");
        print("Money back: " + moneyBack + " HUF");
        printBalance();
        SoundManager.playCoinSound();
    }

    public void setCoffee(Coffee coffee) {
        mCurrentCoffee = coffee;
        printCoffee();
        setExtraButton(mCurrentCoffee instanceof AcceptsExtras ? true : false);
    }

    public void getCoffee() {
        if (!isCoffeeInProgress && checkCoffee() && hasEnoughMoney(mCurrentCoffee.getCost())) {
            isCoffeeInProgress = true;
            SoundManager.playCoffeeMachineSound();
            if (outExtrasFromStore()) {
                mMoney -= mCurrentCoffee.getCost();
                print("Making a coffee...\nIngredients: " + mCurrentCoffee.getIngredientsString());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        print(mCurrentCoffee.getName() + " done.");
                        printBalance();
                        mCurrentCoffee = null;
                        SoundManager.stopCoffeeMachineSound();
                        isCoffeeInProgress = false;
                    }
                }, mCurrentCoffee.getPreparingTime());
            } else {
                print("Not enough ingredients");
            }
        }
    }

    private boolean outExtrasFromStore() {
        Store store = Store.getInstance();
        for (Ingredient ingredient : mCurrentCoffee.getIngredients()) {
            if (ingredient instanceof ExtraIngredient) {
                try {
                    store.getExtraIngredientFromStore((ExtraIngredient) ingredient);
                } catch (StoreProductException e) {
                    printConsoleLog(ingredient.getName() + " " + e.getMessage());
                    return false;
                }
            }
        }
        return true;
    }

    private boolean hasEnoughMoney(int cost) {
        int tmp = mMoney;
        tmp -= cost;
        boolean hasEnoughMoney = tmp >= 0;
        if (!hasEnoughMoney) {
            print("You don't have enough money!");
        }
        return hasEnoughMoney;
    }

    public void addExtra(ExtraIngredient extra) throws CoffeeNotAcceptsExtrasException {
        if (checkCoffee() && hasEnoughMoney(mCurrentCoffee.getTotalCost() + extra.getCost())) {
            // TODO: implement
            // TODO: Csak akkor add hozzá az extra hozzávalót, ha van a gépben elegendő pénz rá, ellenkező esetben figyelmeztesd a usert [print("...")]
            // TODO: Dobj hu.centralmediacsoport.coffeemachine.exceptions.CoffeeNotAcceptsExtrasException kivételt, ha az akutális kávé nem fogadja az extrákat
            // TODO: A kivételt kezeld le a hívó metódusban is!
            // TODO: Ha sikerült hozzáadni, akkor azt jelezzük a felhasználónak.

            if (mCurrentCoffee instanceof AcceptsExtras) {
                ((AcceptsExtras) mCurrentCoffee).addExtra(extra);
                mMoney -= extra.getCost();
                printCoffee();
            } else {
                CoffeeNotAcceptsExtrasException expection = new CoffeeNotAcceptsExtrasException();
                print(expection.getMessage());
                throw expection;
            }
        }
    }


    private boolean checkCoffee() {
        boolean hasCoffee = mCurrentCoffee != null;
        if (!hasCoffee) {
            print("Select a coffee before create!");
        }
        return hasCoffee;
    }

    private void print(@NonNull String message) {
        EventBus.getDefault().postSticky(new CoffeeMachineStatusMessage(message));
    }

    private void printCoffee() {
        if (mCurrentCoffee != null) {
            // TODO: Írja ki: _KÁVÉ_NEVE_ [ + _EXTRA1_ + _EXTRA2_ ] - Any extras? \n _KÁVÉ_ÖSSZ_ÁRA_ HUF.
            // TODO: pl.: Espresso + Milk + Sugar - Any extras? \n 123 HUF
            // TODO: pl.: Espresso - Any extras? \n 89 HUF
            // TODO: pl.: Cappuccino \n 234 HUF
            // TODO: csak akkor írja ki az "Any extras" szöveget, ha a kávé valóban képes extra hozzávalók fogadására
            // TODO: FONTOS! Az alábbi soron nem változtathatsz!
            print(String.valueOf(mCurrentCoffee));
        }
    }

    private void printBalance() {
        printConsoleLog("User's current balance is " + mMoney + " HUF");
        EventBus.getDefault().postSticky(new CoffeeMachineBalanceMessage(String.valueOf(mMoney)));
    }

    private void printConsoleLog(String logMessage) {
        EventBus.getDefault().postSticky(new CoffeeMachineConsoleLogMessage(logMessage));
    }

    private void setExtraButton(boolean setClickablelity) {
        EventBus.getDefault().postSticky(new Boolean(setClickablelity));
    }
}
