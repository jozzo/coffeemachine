package hu.centralmediacsoport.coffeemachine.machine;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import hu.centralmediacsoport.coffeemachine.exceptions.StoreProductException;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.ExtraIngredient;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Milk;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Sugar;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Whip;
import hu.centralmediacsoport.coffeemachine.machine.utils.CounterInt;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineConsoleLogMessage;

/**
 * Created by jozsa on 2017.01.01..
 */

public class Store {

    private HashMap<String, CounterInt> extraIngredients;


    private static Store instance = null;

    public static Store getInstance() {
        if (instance == null) {
            instance = new Store();
        }
        return instance;
    }

    private Store() {
        extraIngredients = new HashMap<>();
        extraIngredients.put(new Milk().getName(), new CounterInt(3));
        extraIngredients.put(new Whip().getName(), new CounterInt(3));
        extraIngredients.put(new Sugar().getName(), new CounterInt(3));
        printConsoleLog("Store is ready");
    }

    public ExtraIngredient getExtraIngredientFromStore(ExtraIngredient ingredient) throws StoreProductException {
        int itemNum = extraIngredients.get(ingredient.getName()).get();
        itemNum -= 1;
        if (itemNum >= 0) {
            extraIngredients.get(ingredient.getName()).decrease();
            printConsoleLog(ingredient.getName() + ": " + itemNum + " in store");
            return ingredient;
        } else {
            throw new StoreProductException();
        }
    }

    private void printConsoleLog(String logMessage) {
        EventBus.getDefault().postSticky(new CoffeeMachineConsoleLogMessage(logMessage));
    }
}
