package hu.centralmediacsoport.coffeemachine.machine.coffees;

import java.util.List;

import hu.centralmediacsoport.coffeemachine.machine.AcceptsExtras;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.GroundCoffee;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.Water;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.ExtraIngredient;

/**
 * Created by david.eperjes on 08/11/16.
 */
//TODO extraIngredients??? a getExtra()-hoz
public class Espresso extends Coffee implements AcceptsExtras {

    public static final int COST = 150;

    public Espresso() {
        ingredients.add(new Water());
        ingredients.add(new GroundCoffee());
    }

    @Override
    public String getName() {
        return "Espresso";
    }

    @Override
    public int getCost() {
        return COST;
    }


    @Override
    public List<ExtraIngredient> getExtras() {
        return null;
    }

    @Override
    public void addExtra(ExtraIngredient extraIngredient) {
        ingredients.add(extraIngredient);
    }

    @Override
    public void removeExtra(ExtraIngredient extraIngredient) {
        ingredients.remove(extraIngredient);
    }

    @Override
    public void removeExtras() {
        ingredients.clear();
    }

    @Override
    public String toString() {
        return getName() + getIngredientsString() + " - Any extras? "+ getTotalCost() +" Ft";
    }
}
