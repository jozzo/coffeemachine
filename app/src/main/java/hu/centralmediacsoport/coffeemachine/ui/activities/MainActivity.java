package hu.centralmediacsoport.coffeemachine.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import hu.centralmediacsoport.coffeemachine.adapters.ConsoleAdapter;
import hu.centralmediacsoport.coffeemachine.entities.ConsoleLogMessage;
import hu.centralmediacsoport.coffeemachine.exceptions.CoffeeNotAcceptsExtrasException;
import hu.centralmediacsoport.coffeemachine.machine.CoffeeMachine;
import hu.centralmediacsoport.coffeemachine.machine.coffees.Cappuccino;
import hu.centralmediacsoport.coffeemachine.machine.coffees.Coffee;
import hu.centralmediacsoport.coffeemachine.machine.coffees.Espresso;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Milk;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Sugar;
import hu.centralmediacsoport.coffeemachine.machine.ingredients.extras.Whip;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineBalanceMessage;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineConsoleLogMessage;
import hu.centralmediacsoport.coffeemachine.messages.CoffeeMachineStatusMessage;
import hu.centralmediacsoport.coffeemachine.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private CoffeeMachine mMachine = new CoffeeMachine();
    private TextView mStatusDisplay, mBalanceDisplay;
    private ConsoleAdapter mConsoleAdapter;
    private RecyclerView mRecyclerView;
    private Button btnExtra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStatusDisplay = (TextView) findViewById(R.id.display);
        mBalanceDisplay = (TextView) findViewById(R.id.display_balance);

        showAddCoinsDialog();

        setupConsole();
        setupButtons();

        EventBus.getDefault().register(this);
    }

    private void setupConsole() {

        mConsoleAdapter = new ConsoleAdapter(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.console);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mConsoleAdapter);

    }

    private void setupButtons() {
        findViewById(R.id.add_coins).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCoins(300);
            }
        });

        findViewById(R.id.money_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moneyBack();
            }
        });

        findViewById(R.id.select_espresso).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCoffee(new Espresso());
            }
        });

        findViewById(R.id.select_cappuccino).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCoffee(new Cappuccino());
            }
        });


        findViewById(R.id.create_coffee).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCoffee();
            }
        });

        btnExtra = (Button) findViewById(R.id.add_extra);
        btnExtra.setAlpha(0.5f);
        btnExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showExtras();
            }
        });

    }

    private void addCoins(int value) {
        mMachine.pay(value);
    }

    private void moneyBack() {
        mMachine.payBack();
    }

    private void setCoffee(Coffee coffee) {
        mMachine.setCoffee(coffee);

    }

    private void createCoffee() {
        mMachine.getCoffee();
    }

    private void showExtras() {


        AlertDialog ad = new AlertDialog.Builder(this)
                .setTitle(R.string.select_extra_title)
                .setItems(R.array.extras, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            switch (i) {
                                case 0: // Milk
                                    mMachine.addExtra(new Milk());
                                    break;

                                case 1: // Sugar
                                    mMachine.addExtra(new Sugar());
                                    break;

                                case 2: // Whip
                                    mMachine.addExtra(new Whip());
                                    break;

                            }
                        } catch (CoffeeNotAcceptsExtrasException e) {
                            Log.d(TAG, e.getMessage());
                        }
                        dialogInterface.dismiss();
                    }
                }).create();
        ad.show();

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onStatusMessage(CoffeeMachineStatusMessage message) {
        mStatusDisplay.setText(message.getMessage());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onBalanceMessage(CoffeeMachineBalanceMessage message) {
        mBalanceDisplay.setText(message.getMessage());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onConsoleMessage(CoffeeMachineConsoleLogMessage message) {
        printConsole(message.getMessage());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void setExtraButton(Boolean active) {
        btnExtra.setClickable(active);
        btnExtra.setAlpha(active ? 1.0f : 0.5f);
    }

    private void printConsole(String message) {
        mConsoleAdapter.addMessage(new ConsoleLogMessage(message));
        mRecyclerView.scrollToPosition(mConsoleAdapter.getItemCount() - 1);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

//    class OnCoffeeCreate implements View.OnClickListener {
//
//        private Coffee coffee;
//
//        public OnCoffeeCreate(Coffee coffee) {
//            this.coffee = coffee;
//        }
//
//        @Override
//        public void onClick(View view) {
//            setCoffee(coffee);
//        }
//    }


    private void showAddCoinsDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        final EditText edittext = new EditText(this);
        edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
        dialog.setTitle(getString(R.string.init_dialog_title));

        dialog.setView(edittext);

        dialog.setPositiveButton(getString(R.string.init_dialog_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                int number = getNumber(edittext.getText().toString());
                if (number > 0) {
                    addCoins(number);
                    mStatusDisplay.setText("Hi!");
                } else {
                    Toast.makeText(MainActivity.this, "Not valid coins", Toast.LENGTH_LONG).show();
                }
            }
        });

        dialog.setNegativeButton(getString(R.string.init_dialog_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        dialog.show();

    }

    private int getNumber(String numberText) {
        if (numberText != null && !"".equals(numberText)) {
            if (TextUtils.isDigitsOnly(numberText)) {
                return Integer.parseInt(numberText);
            }
        }
        return -1;
    }

}
